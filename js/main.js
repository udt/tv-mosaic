window.onload = function () {
  document.getElementById('mobile-open-menu').onclick = function(event) {
      event.preventDefault();
      document.getElementById('mobile-menu-layers').className += ' active-menu-layers';
      document.body.style.overflow = 'hidden';
  };
  document.getElementById('mobile-close-menu').onclick = function(event) {
      event.preventDefault();
      document.getElementById('mobile-menu-layers').className = 'mobile-menu-layers';
      document.body.style.overflow = 'auto';
  };
    document.getElementById('close-column').onclick = function() {
        document.getElementById('mobile-menu-layers').className = 'mobile-menu-layers';
        document.body.style.overflow = 'auto';
    };
    document.getElementById('menu-shadow').onclick = function() {
        document.getElementById('mobile-menu-layers').className = 'mobile-menu-layers';
        document.body.style.overflow = 'auto';
    };
  document.getElementById('dropdown-toggle').onclick = function(event) {
      event.preventDefault();
      document.getElementById('desktop-dropdown-menu').style.display =
          (document.getElementById('desktop-dropdown-menu').style.display == 'block') ? 'none' : 'block';
  };
    document.getElementById('mobile-dropdown-toggle').onclick = function(event) {
        event.preventDefault();
        document.getElementById('mobile-dropdown-menu').style.display =
            (document.getElementById('mobile-dropdown-menu').style.display == 'block') ? 'none' : 'block';
    };
};